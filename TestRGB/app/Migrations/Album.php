<?php

namespace app\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;

class Album
{
	public function up()
	{
		Capsule::schema()->create('album', function($table){

			$table->increments("id");
			$table->string("nome");
			$table->string("descricao");
			$table->string("img");
			
		});

	}

	public  function down(){
		Capsule::schema()->drop('album');
	}





}