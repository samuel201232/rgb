-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: rgb
-- ------------------------------------------------------
-- Server version	8.0.23-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `album` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/1.png'),(2,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/2.png'),(3,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/3.png'),(4,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/4.png'),(5,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/5.png'),(6,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/6.png'),(7,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/7.png'),(8,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/8.png'),(9,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/9.png'),(10,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/10.png'),(11,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/11.png'),(12,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/12.png'),(13,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/13.png'),(14,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/14.png'),(15,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/15.png'),(16,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/16.png'),(17,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/17.png'),(18,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/18.png'),(19,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/19.png'),(20,'Nome do Álbum lorem Ipsum Dolor Amed','Nome do Álbum lorem Ipsum Dolor Amed ','img/20.png');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rgb'
--

--
-- Dumping routines for database 'rgb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-09 15:24:22
